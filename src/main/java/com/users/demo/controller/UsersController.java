package com.users.demo.controller;

import com.users.demo.exception.ResourceNotFoundException;
import com.users.demo.model.Users;
import com.users.demo.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.Date;
import java.util.Optional;

@Controller
@RequestMapping("/users")
public class UsersController {

    @Autowired
    UsersRepository usersRepository;

    @GetMapping
    public String list(Model model) {
        model.addAttribute("usersList", usersRepository.findAll());
        return "users/list";
    }

    @GetMapping("/create")
    public String create(Model model) {
        model.addAttribute("user", new Users());
        return "users/create";
    }

    @GetMapping("/edit/{id}")
    public String edit(@PathVariable(value = "id") long id, Model model) {
        model.addAttribute("user", usersRepository.findById(id).orElseGet(Users::new));
        return "users/update";
    }

    @GetMapping("/delete/{id}")
    public RedirectView delete(@PathVariable(value = "id") long id) {
        usersRepository.deleteById(id);
        return new RedirectView("/users", true);
    }

    @PostMapping("/create")
    public RedirectView postCreate(@ModelAttribute("user") Users user) {
        user.setCreatedAt(new Date());
        usersRepository.save(user);
        return new RedirectView("/users", true);
    }

    @PostMapping("/update/{id}")
    public RedirectView postUpdate(@PathVariable(value = "id") long id, @ModelAttribute("user") Users user) {
        Users data = usersRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Users", "id", id));

        data.setFullName(user.getFullName());
        data.setBirthDate(user.getBirthDate());
        data.setGender(user.getGender());
        data.setCity(user.getCity());
        data.setUpdatedAt(new Date());
        usersRepository.save(data);
        return new RedirectView("/users", true);
    }
}
