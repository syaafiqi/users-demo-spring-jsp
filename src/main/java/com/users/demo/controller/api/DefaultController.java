package com.users.demo.controller.api;

import com.users.demo.dto.response.ApiResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class DefaultController {
    @GetMapping
    public ApiResponse<String> testAPI() {
        return new ApiResponse<>("API WORKS!", true, null);
    }
}
