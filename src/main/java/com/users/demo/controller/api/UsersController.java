package com.users.demo.controller.api;

import com.users.demo.dto.request.LoginRequest;
import com.users.demo.dto.response.ApiResponse;
import com.users.demo.exception.ResourceNotFoundException;
import com.users.demo.model.Users;
import com.users.demo.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController("ApiUsersController")
@RequestMapping("/api/users")
public class UsersController {

    @Autowired
    UsersRepository usersRepository;

    @PostMapping("/login")
    public ResponseEntity<ApiResponse<?>> login(@Valid @RequestBody LoginRequest request) {
        Optional<Users> user = usersRepository.findByFullNameIgnoreCase(request.getUsername());
        if (user.isEmpty()) {
            return new ResponseEntity<>(new ApiResponse<>("User not found", false, null), HttpStatus.NOT_FOUND);
        }

        if (request.getPassword().equals("123456")) {
            return new ResponseEntity<>(new ApiResponse<>("User found", true, user.get()), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new ApiResponse<>("Wrong password", false, null), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    public ApiResponse<List<Users>> getAll() {
        return new ApiResponse<>("success", true, usersRepository.findAll());
    }

    @PostMapping
    public ApiResponse<Users> create(@Valid @RequestBody Users user) {
        user.setCreatedAt(new Date());
        return new ApiResponse<>("success", true, usersRepository.save(user));
    }

    @GetMapping("/{id}")
    public ApiResponse<Users> getById(@PathVariable(value = "id") Long id) {
        return new ApiResponse<>("success", true, usersRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Users", "id", id)));
    }

    @PutMapping()
    public ApiResponse<Users> update(@PathVariable(value = "id") Long id,
                                     @Valid @RequestBody Users user) {

        Users data = usersRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Users", "id", id));

        data.setFullName(user.getFullName());
        data.setBirthDate(user.getBirthDate());
        data.setGender(user.getGender());
        data.setCity(user.getCity());
        data.setUpdatedAt(new Date());

        return new ApiResponse<>("success", true, usersRepository.save(data));
    }

    @DeleteMapping("/{id}")
    public ApiResponse<?> delete(@PathVariable(value = "id") Long id) {
        Users data = usersRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Users", "id", id));

        usersRepository.delete(data);

        return new ApiResponse<>("success", true, null);
    }
}

