<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="shortcut icon" type="image/png" href="/favicon.png">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
              crossorigin="anonymous">

        <link rel="stylesheet" href="/styles.css">
        <title>Home - Spring Boot + JSP</title>
    </head>
    <body>
    <div class="container">

          <div class="title">
            <h1>Home</h1>
          </div>

          <div> This is the HOME page </div>

          <br>

          <div> Navigate: </div>
          <ul>
            <li>
                <a href="users">Users Page</a>
            </li>
          </ul>
    </div>
    </body>
</html>