<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="shortcut icon" type="image/png" href="/favicon.png">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
          crossorigin="anonymous">

    <link rel="stylesheet" href="/styles.css">
    <title>Users - Spring Boot + JSP</title>

</head>

<body>

   <div class="container">

      <div class="title">
        <h1>Users</h1>
      </div>

      <a class="btn btn-success" href="${pageContext.request.contextPath}/users/create" role="button">Add Data</a>

      <table class="table">
          <thead>
              <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Name</th>
                  <th scope="col">Birth Date</th>
                  <th scope="col">Gender</th>
                  <th scope="col">City</th>
                  <th scope="col">Actions</th>
              </tr>
          </thead>
          <tbody>
              <c:choose>
                  <c:when test="${not empty usersList}">
                      <c:forEach var="item" items="${usersList}">
                          <tr>
                              <th scope="row">${item.id}</th>
                              <td>${item.fullName}</td>
                              <td><fmt:formatDate type = "date" pattern = "dd MMM yyyy" value = "${item.birthDate}" /></td>
                              <td>${item.gender}</td>
                              <td>${item.city}</td>
                              <td>
                                  <a class="btn btn-primary btn-sm" href="${pageContext.request.contextPath}/users/edit/${item.id}" role="button">Edit</a>
                                  <a class="btn btn-danger btn-sm" href="${pageContext.request.contextPath}/users/delete/${item.id}" role="button">Delete</a>
                              </td>
                          </tr>
                      </c:forEach>
                  </c:when>
                  <c:otherwise>
                      <b>NO DATA</b>
                  </c:otherwise>
              </c:choose>
          </tbody>
      </table>
</body>

</html>
