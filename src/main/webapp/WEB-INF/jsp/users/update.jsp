<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/styles.css">

    <div class="title">
        <title>Update User - Spring Boot + JSP</title>
    </div>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.min.css">

</head>
<body>
    <div class="container">
        <h1 class="mt-4">Update User</h1>

        <c:if test="${hasError}">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <h4 class="alert-heading">Error!</h4>
              <p>Failed to insert data.</p>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
        </c:if>

        <form class="mt-4" method="post" action="${pageContext.request.contextPath}/users/update/${user.id}">
            <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="fullName" value="${user.fullName}" required>
            </div>
            <div class="form-group">
                <label for="email">Birth Date:</label>
                <input type="text" class="form-control datepicker" name="birthDate" value="<fmt:formatDate type = 'date' pattern = 'yyyy-MM-dd' value = '${user.birthDate}' />" required>
            </div>
            <div class="form-group">
                <label for="Gender">Gender:</label>
                <select class="form-control" id="gender" name="gender">
                    <option <c:if test="${user.gender == ''}"> selected </c:if>>Select gender...</option>
                    <option value="Male" <c:if test="${user.gender == 'Male'}"> selected </c:if>>Male</option>
                    <option value="Female" <c:if test="${user.gender == 'Female'}"> selected </c:if>>Female</option>
                    <option value="Non-Binary" <c:if test="${user.gender == 'Non-Binary'}"> selected </c:if>>Non-Binary</option>
                </select>
            </div>
            <div class="form-group">
                <label for="City">City:</label>
                <input type="text" class="form-control" name="city" value="${user.city}" required>
            </div>
            <button type="submit" class="btn btn-primary">Update User</button>
        </form>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

    <script>
        $('.datepicker').datepicker({
            autoclose: true,
            format: "yyyy-mm-dd"
        });
    </script>
</body>
</html>
